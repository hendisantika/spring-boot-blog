FROM openjdk:8
ADD target/spring-boot-blog.jar docker-spring-boot-blog.jar
EXPOSE 9000
ENTRYPOINT ["java","-jar","docker-spring-boot-blog.jar"]