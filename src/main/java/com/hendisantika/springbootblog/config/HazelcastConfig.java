package com.hendisantika.springbootblog.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/08/18
 * Time: 21.56
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class HazelcastConfig {
    @Bean
    public Config hazelcastConfig() {
        Config config = new Config();
        config.setInstanceName("hazelcast-cache");

        MapConfig mapConfig = new MapConfig();
        mapConfig.setTimeToLiveSeconds(20);
        mapConfig.setEvictionPolicy(EvictionPolicy.LFU);

        config.getMapConfigs().put("mapConfig", mapConfig);

        return config;
    }
}