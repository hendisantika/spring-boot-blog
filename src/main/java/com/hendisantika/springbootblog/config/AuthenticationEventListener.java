package com.hendisantika.springbootblog.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/08/18
 * Time: 21.30
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {

    private static Logger logger = LogManager.getLogger(AuthenticationEventListener.class);

    @Autowired
    RedisTemplate<String, Object> template;

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
        if (authenticationEvent instanceof InteractiveAuthenticationSuccessEvent) {
            // ignores to prevent duplicate logging with
            // AuthenticationSuccessEvent
            return;
        }
        Authentication authentication = authenticationEvent.getAuthentication();
        logger.info(authentication.getDetails());
        logger.info(authentication.getPrincipal());
        String auditMessage = "Login attempt with username: " + authentication.getName() + " Success: "
                + authentication.isAuthenticated();

        logger.info(auditMessage);
        if (authentication.isAuthenticated()) {
            template.opsForValue().set("loggedInEmail", authentication.getName());
        }
    }

}