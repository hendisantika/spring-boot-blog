package com.hendisantika.springbootblog.helper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/08/18
 * Time: 08.59
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PasswordHelper {

    //@Autowired
    //private BCryptPasswordEncoder enc;

    PasswordEncoder enc = new BCryptPasswordEncoder();

    public boolean decryptPassword(String rawPassword, String hashedPassword) {
        return enc.matches(rawPassword, hashedPassword);
    }

    public String hashPassword(String plainPassword) {
        return enc.encode(plainPassword);
    }
}