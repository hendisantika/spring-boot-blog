package com.hendisantika.springbootblog.helper;

import com.hendisantika.springbootblog.config.HttpSessionConfig;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/08/18
 * Time: 08.58
 * To change this template use File | Settings | File Templates.
 */
public class HttpSessionInitializer extends AbstractHttpSessionApplicationInitializer {

    public HttpSessionInitializer() {
        super(HttpSessionConfig.class);
    }
}