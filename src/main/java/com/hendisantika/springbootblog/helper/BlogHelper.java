package com.hendisantika.springbootblog.helper;

import com.hendisantika.springbootblog.domain.Blog;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/08/18
 * Time: 22.32
 * To change this template use File | Settings | File Templates.
 */
public class BlogHelper {
    public static List<Blog> replaceSpaceWithHypen(List<Blog> blogList) {
        for (int i = 0; i < blogList.size(); i++) {
            blogList.get(i).setTitleWithHypen(blogList.get(i).getTitle().replace(" ", "-"));
        }
        return blogList;
    }
}