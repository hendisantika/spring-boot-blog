package com.hendisantika.springbootblog.controller;

import com.hendisantika.springbootblog.domain.Category;
import com.hendisantika.springbootblog.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/08/18
 * Time: 22.12
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class CategoryController {

    private static final String INDEX_PAGE = "category/index";
    private static final String ADD_CATEGORY = "category/addCategory";
    private static final String EDIT_CATEGORY = "category/editCategory";

    @Autowired
    CategoryService categoryService;

    @GetMapping("/category/all")
    public String index(Model model) {
        model.addAttribute("categories", categoryService.findAllCategory());
        return INDEX_PAGE;
    }

    @GetMapping("/category/new")
    public String addCategory(Model model, Category category) {
        return ADD_CATEGORY;
    }

    @PostMapping("/category/new")
    public String addCategory(@Valid Category category, BindingResult result, Model model) {
        model.addAttribute(category);
        if (result.hasErrors()) {
            model.addAttribute("invalidCategory", true);
            return ADD_CATEGORY;
        }

        Category name = categoryService.checkDuplicateCategory(category.getName());
        if (name != null) {
            model.addAttribute("categoryExists", "category.name.exists");
        } else {
            categoryService.saveCategory(category);
            model.addAttribute("categorySaved", "category.save.success");
        }
        return ADD_CATEGORY;
    }

    @GetMapping("/category/edit/{id}")
    public String editCategory(Model model, @PathVariable Long id) {
        Optional<Category> category = categoryService.findCategory(id);
        if (category != null) {
            model.addAttribute(category);
            return EDIT_CATEGORY;
        }
        model.addAttribute("categoryNotFound", true);
        model.addAttribute("categories", categoryService.findAllCategory());
        return INDEX_PAGE;
    }

    @PostMapping(value = "/category/edit/{id}")
    public String editCategory(@Valid Category category, BindingResult result, Model model,
                               @PathVariable Long id) {
        category.setId(id);
        model.addAttribute(category);
        if (result.hasErrors()) {
            model.addAttribute("invalidCategory", true);
            return ADD_CATEGORY;
        }

        if (!result.hasErrors()) {
            Category name = categoryService.checkDuplicateCategory(category.getName());
            if (name != null) {
                model.addAttribute(category);
                model.addAttribute("categoryExists", "category.name.exists");
                return EDIT_CATEGORY;
            } else {
                categoryService.updateCategory(category);
                model.addAttribute("categoryUpdated", true);
            }
        }

        return EDIT_CATEGORY;
    }
}

