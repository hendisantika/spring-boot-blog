package com.hendisantika.springbootblog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/08/18
 * Time: 07.19
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class SettingController {
    private static final String INDEX_PAGE = "settings/index";
    Logger log = LoggerFactory.getLogger(SettingController.class);

    @GetMapping("/settings/user")
    public String index(Principal principal) {
        log.info(principal.getName());
        return INDEX_PAGE;
    }
}