package com.hendisantika.springbootblog.controller;

import com.hendisantika.springbootblog.domain.Resume;
import com.hendisantika.springbootblog.service.ResumeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/08/18
 * Time: 07.18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ResumeController {
    private static final String ADD_RESUME = "resume/addResume2";
    private static final String EDIT_RESUME = "resume/editResume";
    Logger log = LoggerFactory.getLogger(ResumeController.class);
    @Autowired
    private ResumeService resumeService;

    @GetMapping("/resume/new")
    public String addResume(Resume resume, Model model) {
        model.addAttribute("resume", resume);
        return ADD_RESUME;
    }

    @PostMapping("/resume/new")
    public String addResume(@Valid Resume resume, BindingResult result, Model model,
                            Principal principal) {

        resume.setEmail(principal.getName());

		/*
		 * Disable this functionality temporarily
		 *
		List<Career> careerList = new ArrayList<>();
		Career career;
		for(int i=0;i<resume.getCareer().size();i++){
			career=new Career();
			career.setName(resume.getCareer().get(i).getName());
			career.setName(resume.getCareer().get(i).getTitle());
			career.setJoinDate(resume.getCareer().get(i).getJoinDate());
			career.setEndDate(resume.getCareer().get(i).getEndDate());
			career.setLocation(resume.getCareer().get(i).getLocation());
			career.setWebsite(resume.getCareer().get(i).getWebsite());
			career.setCreateDate(LocalDate.now());


			careerList.add(career);
			career.setResume(resume);
		}

		System.out.println("education size "+resume.getEducation().size());
		List<Education> educationList = new ArrayList<>();
		Education education ;
		for(int i=0;i<educationList.size();i++){
			education = new Education();
			education.setName(resume.getEducation().get(i).getName());
			education.setCourse(resume.getEducation().get(i).getCourse());
			education.setStartDate(resume.getEducation().get(i).getStartDate());
			education.setEndDate(resume.getEducation().get(i).getEndDate());


			educationList.add(education);
			education.setResume(resume);
		}

		resume.setCareer(careerList);
		resume.setEducation(educationList);
		*/
        resumeService.save(resume);

        model.addAttribute("resume", resume);
        model.addAttribute("resumeSaved", true);
        return ADD_RESUME;
    }

    @GetMapping("/resume/edit")
    public String addResume(Resume resume, Model model, Principal principal) {
        resume = resumeService.findByEmail(principal.getName());
        resume.setEmail(principal.getName());

        model.addAttribute("resume", resume);
        return EDIT_RESUME;
    }

    @PostMapping("/resume/edit")
    public String editResume(@Valid Resume resume, BindingResult result, Model model, Principal principal) {
        resume.setEmail(principal.getName());
        resumeService.update(resume);

        model.addAttribute("resume", resume);
        model.addAttribute("resumeUpdated", true);

        return EDIT_RESUME;
    }

}


