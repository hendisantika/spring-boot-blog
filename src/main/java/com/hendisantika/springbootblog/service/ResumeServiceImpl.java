package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Resume;
import com.hendisantika.springbootblog.repository.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.18
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeRepository resumeRepository;

    @Override
    public Resume save(Resume resume) {
        return resumeRepository.save(resume);
    }

    @Override
    public Resume update(Resume resume) {
        return resumeRepository.save(resume);
    }

    @Override
    public Resume findByEmail(String email) {
        return resumeRepository.findByEmail(email);
    }

}