package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Role;
import com.hendisantika.springbootblog.domain.User;
import com.hendisantika.springbootblog.repository.RoleRepository;
import com.hendisantika.springbootblog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.20
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User findUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    public User saveUser(User user) {

        Role role = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Arrays.asList(role)));

        return userRepository.save(user);
    }

    @Override
    public User findUserByAuthenticationCode(String authenticationCode) {
        return userRepository.findByAuthenticationCode(authenticationCode);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

}
