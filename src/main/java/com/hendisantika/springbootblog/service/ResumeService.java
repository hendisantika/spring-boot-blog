package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Resume;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.08
 * To change this template use File | Settings | File Templates.
 */
public interface ResumeService {
    Resume save(Resume resume);

    Resume update(Resume resume);

    Resume findByEmail(String email);
}
