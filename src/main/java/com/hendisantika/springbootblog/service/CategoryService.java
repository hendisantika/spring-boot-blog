package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Category;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.05
 * To change this template use File | Settings | File Templates.
 */
public interface CategoryService {
    Category saveCategory(Category category);

    Category checkDuplicateCategory(String name);

    List<Category> findAllCategory();

    Optional<Category> findCategory(Long id);

    void updateCategory(Category category);
}
