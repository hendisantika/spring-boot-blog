package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Contact;
import com.hendisantika.springbootblog.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.18
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Contact saveContact(Contact contact) {
        return contactRepository.save(contact);
    }

    @Override
    public List<Contact> findAllContact() {
        return contactRepository.findAllByOrderByDateDesc();
    }

    @Override
    public Optional<Contact> findContactById(Long id) {
        return contactRepository.findById(id);
    }

}