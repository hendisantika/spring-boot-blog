package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.User;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    User findUserByEmail(String email);

    User saveUser(User user);

    User findUserByAuthenticationCode(String authenticationCode);

    User updateUser(User user);
}
