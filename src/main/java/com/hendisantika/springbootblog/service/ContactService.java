package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Contact;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.07
 * To change this template use File | Settings | File Templates.
 */
public interface ContactService {
    List<Contact> findAllContact();

    Contact saveContact(Contact contact);

    Optional<Contact> findContactById(Long id);
}