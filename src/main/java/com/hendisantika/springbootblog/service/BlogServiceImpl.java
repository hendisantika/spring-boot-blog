package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Blog;
import com.hendisantika.springbootblog.domain.Category;
import com.hendisantika.springbootblog.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.12
 * To change this template use File | Settings | File Templates.
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<Blog> findAllBlogs() {
        return blogRepository.findAll();
    }

    @Override
    public Blog saveBlog(Blog blog) {
        return blogRepository.save(blog);
    }

    @Override
    public Optional<Blog> findBlogById(Long id) {
        return blogRepository.findById(id);
    }

    @Override
    public Blog updateBlog(Blog blog) {
        return blogRepository.save(blog);
    }

    public void deleteBlog(Long id) {
        blogRepository.deleteById(id);
    }

    @Override
    public List<Blog> findRecentBlog() {
        return blogRepository.findFirst2ByOrderByCreateDateDesc();
    }

    @Override
    public List<Blog> findAllBlogsByCategory(Category category) {
        return blogRepository.findBlogByCategory(category);
    }

    @Override
    public List<Blog> findLatest5Blog() {
        return blogRepository.findFirst5ByOrderByCreateDateDesc();
    }

}