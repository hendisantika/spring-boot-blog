package com.hendisantika.springbootblog.service;

import com.hendisantika.springbootblog.domain.Blog;
import com.hendisantika.springbootblog.domain.Category;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.03
 * To change this template use File | Settings | File Templates.
 */
public interface BlogService {
    List<Blog> findAllBlogs();

    Blog saveBlog(Blog blog);

    Optional<Blog> findBlogById(Long id);

    Blog updateBlog(Blog blog);

    void deleteBlog(Long id);

    List<Blog> findRecentBlog();

    List<Blog> findAllBlogsByCategory(Category category);

    List<Blog> findLatest5Blog();
}