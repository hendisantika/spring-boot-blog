package com.hendisantika.springbootblog.service;

import com.google.common.collect.Lists;
import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/08/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
@Service
public class EmailHelperService {
    private static final String SUBJECT = "New User Registration";
    private static final String ENCODING = "UTF-8";
    private static final String FROM = "info@example.com";
    private static final String FROM_NAME = "Hendi Santika";
    @Autowired
    public EmailService emailService;
    Logger log = LoggerFactory.getLogger(EmailHelperService.class);

    public void sendEmailWithoutTemplating(String registeredEmail, String messageBody, String password) throws Exception {
        final Email email = DefaultEmail.builder()
                .from(new InternetAddress(FROM, FROM_NAME))
                .to(Lists.newArrayList(new InternetAddress(registeredEmail, "")))
                .subject(SUBJECT)
                .body("Please click the link to activate your account within 24 hours. " + messageBody + " Your passwod is " + password)
                .encoding(ENCODING).build();

        emailService.send(email);
    }
}