package com.hendisantika.springbootblog.domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/08/18
 * Time: 08.53
 * To change this template use File | Settings | File Templates.
 */
public class Register {
    @Email
    @NotEmpty(message = "{email.invalid}")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
