package com.hendisantika.springbootblog.repository;

import com.hendisantika.springbootblog.domain.Resume;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/08/18
 * Time: 07.14
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface ResumeRepository extends CrudRepository<Resume, Long> {
    Resume findByEmail(String email);
}
