package com.hendisantika.springbootblog.repository;

import com.hendisantika.springbootblog.domain.Blog;
import com.hendisantika.springbootblog.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/08/18
 * Time: 09.00
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {
    List<Blog> findFirst2ByOrderByCreateDateDesc();

    List<Blog> findBlogByCategory(Category category);

    List<Blog> findFirst5ByOrderByCreateDateDesc();
}